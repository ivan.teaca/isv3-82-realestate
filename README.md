### How to run API:

1. Clone the repo and enter the project folder

```bash
git clone git@gitlab.com:ivan.teaca/isv3-82-realestate.git
```
```bash
cd isv3-82-realestate
```

2. Launch the application using Docker Compose in detached mode:

```bash
docker-compose up -d
```

3. Setup database:

```bash
docker-compose exec app rails db:setup
```

That's it. Now you can use it. Open:

http://0.0.0.0:3000/apartments

http://0.0.0.0:3000/apartments/1

...

---
### Swagger doc available on:

http://0.0.0.0:3000/api-docs/index.html

---

To stop and remove the Docker containers, use the following command:

```bash
docker-compose down
```
