# frozen_string_literal: true

apartments = [
  { country: 'UK', description: 'Luxury living with stunning city views' },
  { country: 'USA', description: 'Elegant urban apartments with modern amenities' },
  { country: 'Italy', description: 'Chic apartments with resort-style amenities' },
  { country: 'Spain', description: 'Contemporary living in a vibrant community' },
  { country: 'Canada', description: 'Spacious apartments in the heart of downtown' }
]

apartments.each do |item_info|
  Apartment.create(item_info)
end
