# frozen_string_literal: true

class CreateApartments < ActiveRecord::Migration[7.0]
  def change
    create_table :apartments do |t|
      t.string :country
      t.string :description

      t.timestamps
    end
  end
end
