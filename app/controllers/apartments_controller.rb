# frozen_string_literal: true

class ApartmentsController < ApplicationController
  def index
    @apartments = Apartment.all
    render json: @apartments
  end

  def show
    @apartment = Apartment.find(params[:id])
    render json: @apartment
  end

  def create
    @apartment = Apartment.create(
      country: params[:country],
      description: params[:description]
    )
    render json: @apartment
  end

  def update
    @apartment = Apartment.find(params[:id])
    @apartment.update(
      country: params[:country],
      description: params[:description]
    )
    render json: @apartment
  end

  def destroy
    @apartments = Apartment.all
    @apartment = Apartment.find(params[:id])
    @apartment.destroy
    render json: @apartments
  end
end
